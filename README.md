
# Android Live Currency Conversion Rates 🤑

This repository uses Revolut API to show conversion rates , Followed by Google I/O ❤️  [guidelines](https://developer.android.com/topic/libraries/architecture/index.html) written in Kotlin

## Run the app 🚀
-   Make sure you have latest gradle plugin and Android Studio
-   Build the app
-   hit run button  🎉

## Run Test ✅
-   Build the app
-   In "Project" panel (CMD+1 to show) Right click on "(test)package".
    Click "Run tests in package


## Thanks ✨
![](giphy.webp)
![Alt Text](https://media.giphy.com/media/fxNXSywcvDM5VUdL6d/giphy.gif)
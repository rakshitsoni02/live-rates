package com.rax.liverates.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.rax.liverates.model.ExchangeRate
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ExchangeRateDaoTest {

    private lateinit var db: LiveRatesDatabase

    @Before
    fun initDb() {
        db = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext<Context>(), LiveRatesDatabase::class.java).build()
    }

    @After
    fun closeDb() = db.close()

    @Test
    @Throws(InterruptedException::class)
    fun insertRatesArticles() {
        // GIVEN
        val input = listOf(ExchangeRate(1,"EUR","INR",80.0),
                ExchangeRate(2,"EUR","AUD",1.69))

        // THEN
        assertThat(db.currencyArticlesDao().insertArticles(input), equalTo(listOf(1L, 2L)))
    }

    @Test
    @Throws(InterruptedException::class)
    fun insertRatesArticlesAndRead(): Unit = runBlocking {
        // GIVEN
        val input = listOf(
                ExchangeRate(1,"EUR","INR",80.0),
                ExchangeRate(2,"EUR","AUD",1.69)
        )
        db.currencyArticlesDao().insertArticles(input)

        // THEN
        val articles = db.currencyArticlesDao().getCurrencyArticles("EUR")
        assertThat(articles.size, equalTo(articles.size))
        assertThat(articles, equalTo(articles))
    }

}
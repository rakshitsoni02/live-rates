package com.rax.liverates.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rax.liverates.di.base.ViewModelFactory
import com.rax.liverates.di.base.ViewModelKey
import com.rax.liverates.ui.viewmodel.LiveRatesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Describes all the [ViewModel] which need to be
 * created using DI.
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LiveRatesViewModel::class)
    abstract fun bindRatesArticleViewModel(liveRatesViewModel: LiveRatesViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}

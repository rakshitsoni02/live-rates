package com.rax.liverates.di

import android.app.Application
import com.rax.liverates.LiveRatesApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            // Dagger support
            AndroidInjectionModule::class,

            // App
            CurrencyDatabaseModule::class,
            RatesServiceModule::class,
            ActivityModule::class,
            ViewModelModule::class
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(liveRatesApp: LiveRatesApp)
}

package com.rax.liverates.di.base

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable

package com.rax.liverates.di

import android.app.Application
import com.rax.liverates.db.CurrencyArticlesDao
import com.rax.liverates.db.LiveRatesDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CurrencyDatabaseModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): LiveRatesDatabase = LiveRatesDatabase.buildDefault(app)

    @Singleton
    @Provides
    fun provideUserDao(db: LiveRatesDatabase): CurrencyArticlesDao = db.currencyArticlesDao()
}
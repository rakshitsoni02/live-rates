package com.rax.liverates.api

import com.rax.liverates.model.CurrencySourceResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Fetch all the latest rates article from server
 * using the Rates API.
 */
interface CurrencyService {

    /**
     * Retrieves all the latest exchange rates article from Rates API.
     */
    @GET("latest")
    suspend fun getLatestRates(@Query("base") value: String): CurrencySourceResponse

}

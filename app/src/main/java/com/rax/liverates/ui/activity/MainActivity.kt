package com.rax.liverates.ui.activity

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.rax.liverates.R
import com.rax.liverates.adapter.CurrencyArticlesAdapter
import com.rax.liverates.model.ViewState
import com.rax.liverates.ui.base.BaseActivity
import com.rax.liverates.ui.viewmodel.LiveRatesViewModel
import com.rax.liverates.utils.getViewModel
import com.rax.liverates.utils.hideKb
import com.rax.liverates.utils.observeNotNull
import com.rax.liverates.utils.toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.progress_layout.*


class MainActivity : BaseActivity() {

    private val liveRatesViewModel by lazy { getViewModel<LiveRatesViewModel>() }

    /**
     * Starting point of the activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setting up RecyclerView and adapter
        currencyList.setEmptyView(empty_view)
        currencyList.setProgressView(progress_view)
        val adapter = CurrencyArticlesAdapter {
            hideKb()
            updateBaseRate(it.currencyName)
        }
//        adapter.setHasStableIds(true)
        currencyList.adapter = adapter
        currencyList.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false)
        (currencyList.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

        // Update the UI on state change
        liveRatesViewModel.getCurrencyArticles().observeNotNull(this) { state ->
            when (state) {
                is ViewState.Success -> adapter.replaceItems(state.data, this)
                is ViewState.Loading -> currencyList.showLoading()
                is ViewState.Error -> toast("Something went wrong ¯\\_(ツ)_/¯ => ${state.message}")
            }
        }
    }

    private fun updateBaseRate(rate: String) {
        LiveRatesViewModel.BASE = rate
    }
}

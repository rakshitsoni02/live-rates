package com.rax.liverates.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.rax.liverates.model.ExchangeRate
import com.rax.liverates.model.ViewState
import com.rax.liverates.repo.CurrencyRepository
import javax.inject.Inject

/**
 * A container for [ExchangeRate] related data to show on the UI.
 */
class LiveRatesViewModel @Inject constructor(
        currencyRepository: CurrencyRepository
) : ViewModel() {


    private val exchangeRate: LiveData<ViewState<List<ExchangeRate>>> = currencyRepository.getCurrencyArticles().asLiveData()

    /**
     * Return rates articles to observeNotNull on the UI.
     */
    fun getCurrencyArticles(): LiveData<ViewState<List<ExchangeRate>>> = exchangeRate

    companion object {
        var BASE: String = "EUR"
    }
}
package com.rax.liverates.model

import com.google.gson.annotations.SerializedName

/**
 * Rate source model describing the source details
 * and the articles under it.
 */
data class CurrencySourceResponse(
        @SerializedName("base")
        val baseCurrency: String = "",
        @SerializedName("date")
        val date: String = "",
        @SerializedName("rates")
        val currencyArticles: HashMap<String, Double> = hashMapOf()
)
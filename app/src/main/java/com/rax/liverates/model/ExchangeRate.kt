package com.rax.liverates.model

import androidx.room.*
import com.google.gson.annotations.SerializedName
import com.rax.liverates.model.ExchangeRate.RateArticles.Column
import com.rax.liverates.model.ExchangeRate.RateArticles.tableName

/**
 * Currency Article Model describing the article details
 * fetched from api source.
 */
@Entity(tableName = tableName, indices = [Index(value = [Column.base, Column.currency],
        unique = true)])
data class ExchangeRate(

        /**
         * Primary key for Room.
         */
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,

        /**
         * live rates for Base currency
         */
        @ColumnInfo(name = Column.base)
        @SerializedName(Column.base)
        val base: String,

        /**
         * name of the currency
         */
        @ColumnInfo(name = Column.currency)
        @SerializedName(Column.currency)
        val currencyName: String,

        /**
         * currentRate of the currency against base currency
         */
        @ColumnInfo(name = Column.rate)
        @SerializedName(Column.rate)
        val currentRate: Double
) {
    @Ignore
    var multipleAmount: Double = 1.0

    object RateArticles {
        const val tableName = "exchange_rates"

        object Column {
            const val id = "id"
            const val base = "base"
            const val currency = "currency"
            const val rate = "rate"
        }
    }
}
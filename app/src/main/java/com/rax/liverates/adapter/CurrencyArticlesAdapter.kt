package com.rax.liverates.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rax.liverates.R
import com.rax.liverates.model.ExchangeRate
import com.rax.liverates.ui.viewmodel.LiveRatesViewModel
import com.rax.liverates.utils.inflate
import com.rax.liverates.utils.isKeyboardActive
import com.rax.liverates.utils.validateToDouble
import kotlinx.android.synthetic.main.row_currency_article.view.*


/**
 * The live rates adapter to show the rates in a list.
 */
class CurrencyArticlesAdapter(
        private val listener: (ExchangeRate) -> Unit
) : RecyclerView.Adapter<CurrencyArticlesAdapter.CurrencyRateHolder>() {
    /**
     * List of currency articles
     */
    private var currencyRates: List<ExchangeRate> = emptyList()

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            TYPE_HEADER
        } else {
            TYPE_ITEM
        }
    }

    /**
     * Inflate the view
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CurrencyRateHolder(parent.inflate(R.layout.row_currency_article), RateChangeListener())

    /**
     * Bind the view with the data
     */
    override fun onBindViewHolder(currencyRateHolder: CurrencyRateHolder, position: Int) {
        if (getItemViewType(position) == TYPE_ITEM)
            currencyRateHolder.bind(
                    currencyRates[position - 1], listener, this)
        else {
            currencyRateHolder.renderBaseCurrency()
        }
    }


    /**
     * Number of items in the list to display
     */
    override fun getItemCount(): Int {
        if (currencyRates.isNotEmpty())
            return currencyRates.size + 1
        return 0
    }

    /**
     * View Holder Pattern
     */
    class CurrencyRateHolder
    (itemView: View, private val rateChangeListener: RateChangeListener)
        : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.etSearchRate.setOnTouchListener(rateChangeListener)
        }

        /**
         * Binds the UI with the data and handles clicks
         */
        fun bind(exchangeRate: ExchangeRate, listener: (ExchangeRate) -> Unit
                 , adapter: CurrencyArticlesAdapter) = with(itemView) {
            rateChangeListener.updatePosition(layoutPosition)
            currencyName.text = exchangeRate.currencyName
            val rate = String.format("%.2f", exchangeRate.currentRate * MULTIPLE)
            tvCurrencyRate.text = exchangeRate.currentRate.toString()
            etSearchRate.setText(rate)
            setOnClickListener {
                MULTIPLE = rate.validateToDouble()
                listener(exchangeRate)
                adapter.notifyItemChanged(0)
            }
        }

        fun renderBaseCurrency() {
            itemView.currencyName.text = LiveRatesViewModel.BASE
            itemView.etSearchRate.setText(String.format("%.2f", MULTIPLE))
            rateChangeListener.updatePosition(layoutPosition)
            itemView.etSearchRate.addTextChangedListener(rateChangeListener)
        }
    }

    /**
     * Swap function to set new data on updating
     */
    fun replaceItems(items: List<ExchangeRate>, ctx: Context) {
        currencyRates = items
        if (ctx.isKeyboardActive()) {
            notifyItemRangeChanged(1, currencyRates.size + 1)
            return
        }
        notifyDataSetChanged()
    }

    inner class RateChangeListener : TextWatcher, View.OnTouchListener {
        override fun onTouch(v: View?, p1: MotionEvent?): Boolean {
            return if (position != 0) {
                v?.isFocusable = false
                v?.isFocusableInTouchMode = false
                true
            } else {
                v?.isFocusable = true
                v?.isFocusableInTouchMode = true
                false
            }
        }

        private var position: Int = 0

        fun updatePosition(position: Int) {
            this.position = position
        }

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            // no op
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            try {
                MULTIPLE = if (charSequence.isNotEmpty())
                    charSequence.toString().validateToDouble()
                else 0.0
            } catch (e: Exception) {
            }

        }

        override fun afterTextChanged(editable: Editable) {
            // no op
        }
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
        private var MULTIPLE: Double = 1.0
    }

}
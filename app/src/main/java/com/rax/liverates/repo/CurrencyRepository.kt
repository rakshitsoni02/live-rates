package com.rax.liverates.repo

import com.rax.liverates.api.CurrencyService
import com.rax.liverates.db.CurrencyArticlesDao
import com.rax.liverates.model.CurrencySourceResponse
import com.rax.liverates.model.ExchangeRate
import com.rax.liverates.model.ViewState
import com.rax.liverates.ui.viewmodel.LiveRatesViewModel.Companion.BASE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository abstracts the logic of fetching the data and persisting it for
 * offline. They are the data source as the single source of truth.
 */
@Singleton
class CurrencyRepository @Inject constructor(
        private val currencyDao: CurrencyArticlesDao,
        private val currencyService: CurrencyService
) {


    /**
     * Fetch the currency articles from database if exist else fetch from web
     * and persist them in the database
     */
    fun getCurrencyArticles(): Flow<ViewState<List<ExchangeRate>>> {
        return flow {
            while (true) {
                delay(1000)
                // 1. Start with loading + data from database
                emit(ViewState.loading())
                emit(ViewState.success(currencyDao.getCurrencyArticles(BASE)))
                // 2. Try fetching new rates -> save + emit
                val currencySource = currencyService.getLatestRates(BASE)
                currencyDao.insertArticles(convertSourceResponse(currencySource))
                // 3. Get articles from database [Single source of truth]
                emit(ViewState.success(currencyDao.getCurrencyArticles(BASE)))
            }

        }.catch {
            emit(ViewState.error(it.message.orEmpty()))
        }.flowOn(Dispatchers.IO)
    }


    private fun convertSourceResponse(currencySource: CurrencySourceResponse): List<ExchangeRate> {
        val result: MutableList<ExchangeRate> = mutableListOf()
        currencySource.currencyArticles.forEach {
            result.add(ExchangeRate(base = currencySource.baseCurrency,
                    currencyName = it.key, currentRate = it.value))
        }
        return result
    }
}
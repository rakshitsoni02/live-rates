package com.rax.liverates.db

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rax.liverates.model.ExchangeRate

@Database(
        entities = [ExchangeRate::class],
        version = 1
)
abstract class LiveRatesDatabase : RoomDatabase() {

    /**
     * Get currency article DAO
     */
    abstract fun currencyArticlesDao(): CurrencyArticlesDao

    companion object {

        private const val databaseName = "currency-db"

        fun buildDefault(context: Context) =
                Room.databaseBuilder(context, LiveRatesDatabase::class.java, databaseName)
                        .build()

        @VisibleForTesting
        fun buildTest(context: Context) =
                Room.inMemoryDatabaseBuilder(context, LiveRatesDatabase::class.java)
                        .build()
    }
}
package com.rax.liverates.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rax.liverates.model.ExchangeRate

/**
 * Defines access layer to rates table
 */
@Dao
interface CurrencyArticlesDao {

    /**
     * Insert articles into the table
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticles(articles: List<ExchangeRate>): List<Long>

    /**
     * Get all the articles from table
     */
    @Query("SELECT * FROM exchange_rates where base=:baseCurrency")
    suspend fun getCurrencyArticles(baseCurrency: String): List<ExchangeRate>

}
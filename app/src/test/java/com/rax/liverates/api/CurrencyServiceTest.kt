package com.rax.liverates.api

import com.rax.liverates.utils.create
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

@RunWith(JUnit4::class)
class CurrencyServiceTest : BaseServiceTest() {

    private lateinit var service: CurrencyService

    @Before
    @Throws(IOException::class)
    fun createService() {
        service = Retrofit.Builder()
                .baseUrl(mockWebServer.url("/"))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create()
    }

    @Test
    @Throws(IOException::class, InterruptedException::class)
    fun getLiveRatesTest() = runBlocking {
        enqueueResponse("currency_source.json")
        val ratesResponse = service.getLatestRates("EUR")

        // Dummy request
        mockWebServer.takeRequest()

        // Check rate source
        assertThat(ratesResponse, notNullValue())
        assertThat(ratesResponse.baseCurrency, `is`("EUR"))
        assert(ratesResponse.currencyArticles.size > 0)

        // Check list
        val articles = ratesResponse.currencyArticles
        assertThat(articles, notNullValue())

        // Check item 1
        val article1 = articles["AUD"]
        assertThat(article1, notNullValue())
        assertThat(article1, `is`(1.6129))
    }
}